<?php
/**
 * Created by PhpStorm.
 * User: David
 */

register_rest_route( 'wp/v2', '/get-all-post-ids/', array(
    'methods' => 'GET',
    'callback' => 'my_rest_api_plugin_get_all_post_ids',
) );

function my_rest_api_plugin_get_all_post_ids() {
    if ( false === ( $all_post_ids = get_transient( 'my_rest_api_plugin_all_post_ids' ) ) ) {
        $all_post_ids = get_posts( array(
            'numberposts' => -1,
            'post_type'   => 'post',
            'fields'      => 'ids',
        ) );
        // cache for 2 hours
        set_transient( 'my_rest_api_plugin_all_post_ids', $all_post_ids, 60*60*2 );
    }

    return $all_post_ids;
}