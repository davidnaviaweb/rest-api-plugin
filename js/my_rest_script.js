/**
 * Created by David on 27/10/2016.
 */

jQuery(document).ready(function($) {

    $('#go_rest_api').click(function() {

        var new_id = $('#new_rest_id').val();
        var new_title = $('#new_rest_title').val();

        jQuery.ajax( {
            url: wpApiSettings.root + 'wp/v2/posts/' + new_id,
            method: 'POST',
            beforeSend: function ( xhr ) {
                xhr.setRequestHeader( 'X-WP-Nonce', wpApiSettings.nonce );
            },
            data:{
                'title' : new_title
            }
        } ).done( function ( response ) {
            $('#rest_result').html(JSON.stringify(response));
            console.log( response );
        } );
    });
});


