<?php
/**
 * Plugin Name:       My Rest API plugin
 * Description:       My Rest API plugin
 * Version:           1.0.0
 * Author:            David Navia
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

if (!defined('WPINC')) {
    die;
}


// Routes
function my_rest_api_plugin_register_routes()
{
    register_rest_route('topics-api/v2', '/(?P<topic>[^\/]+)', array(
        'methods' => WP_REST_Server::READABLE,
        'callback' => 'my_rest_api_plugin_callback',
    ));
    register_rest_route('topics-api/v2', '/(?P<topic>[^\/]+)/(?P<count>\d+)', array(
        'methods' => WP_REST_Server::READABLE,
        'callback' => 'my_rest_api_plugin_callback',
    ));

}

// Callbacks
function my_rest_api_plugin_callback( WP_REST_Request $request )
{


    $parameters = $request->get_params();

    $topic = $parameters['topic'];

    if (preg_match_all('/%[0-9A-F]{2}+/',$topic) > 0){
        $topic = urldecode($topic);
    }

    $count = isset($parameters['count']) ? $parameters['count'] : 1;


    $args = array(
        'post_type' => get_post_types(),
        'posts_per_page' => $count,
        'tax_query' => array(
            'relation' => 'OR',
            array(
                'taxonomy' => 'category',
                'field' => 'slug',
                'terms' => array( $topic )
            ),
            array(
                'taxonomy' => 'post_tag',
                'field' => 'slug',
                'terms' => array( $topic )
            )
        )
    );

    $query = new WP_Query(
        $args
    );

    if ( empty( $query->posts ) ) {
        return new WP_Error( 'no_posts_found', 'No posts found', array( 'status' => 404 ) );
    }else{
        $posts = array();
        foreach ($query->posts as $post) {

            $GLOBALS['post'] = $post;
            setup_postdata($post);

            $item['post_title'] = get_the_title();
            $item['post_content'] = get_the_content();
            $item['post_excerpt'] = get_the_excerpt();
            $item['post_url'] = get_permalink($post->ID);
            $item['post_type'] = get_post_type();
            $item['post_thumbnail'] = get_the_post_thumbnail_url();

            $posts[] = $item;
        }
    }

    wp_reset_postdata();
    return $posts;
}

// Init
function my_rest_api_plugin_init()
{
    my_rest_api_plugin_register_routes();
    include('endpoint-cached.php');
}
add_action('rest_api_init', 'my_rest_api_plugin_init');


//include('post-types.php');
//include('simple-sample.php');



