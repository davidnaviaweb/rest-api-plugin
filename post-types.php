<?php
/**
 * Created by PhpStorm.
 * User: David
 */

function my_rest_api_plugin_register_post_type()
{

    $capabilities = array(
        'publish_posts' => 'moderate_comments',
        'edit_posts' => 'moderate_comments',
        'edit_others_posts' => 'moderate_comments',
        'delete_posts' => 'moderate_comments',
        'delete_others_posts' => 'moderate_comments',
        'read_private_posts' => 'moderate_comments',
        'edit_post' => 'moderate_comments',
        'delete_post' => 'moderate_comments',
        'read_post' => 'moderate_comments',
    );

    $labels = array(
        'name' => 'Noticias',
        'singular_name' => 'Noticia',
        'add_new' => 'Añadir nueva',
        'add_new_item' => 'Añadir nueva noticia',
        'edit' => 'Editar',
        'edit_item' => 'Editar noticia',
        'new_item' => 'Nueva noticia',
        'view' => 'Ver',
        'view_item' => 'Ver noticia',
        'search_items' => 'Buscar noticias',
        'not_found' => 'No se encontraron noticias',
        'not_found_in_trash' => 'No se encontraron noticias en la papelera'
    );

    register_post_type('news',
        array(
            'labels' => $labels,
            'description' => 'Noticias',
            'menu_icon' => 'dashicons-format-aside',
            'menu_position' => 11,
            'public' => true,
            'show_ui' => true,
            'hierarchical' => false,
            'supports' => array('title', 'editor', 'thumbnail', 'author', 'revisions', 'excerpt'),
            'taxonomies' => array('category', 'post_tag'),
            'capabilities' => $capabilities,
            'query_var' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => '', 'with_front' => false),
            'show_in_rest' => true,
            'rest_base' => 'news-api',
            'rest_controller_class' => 'WP_REST_Posts_Controller',
        )
    );
}

add_action('init', 'my_rest_api_plugin_register_post_type');