<?php
/**
 * Created by PhpStorm.
 * User: David
 */

function my_rest_api_plugin_test_page()
{
    add_menu_page("Rest API Test", "Rest API Test", "moderate_comments", "my-rest-api-plugin-test", "my_rest_api_plugin_test_callback",
        'dashicons-smiley');
    wp_enqueue_script( 'my_rest_script', plugins_url( 'js/my_rest_script.js', __FILE__ ), array( 'wp-api' ) );

}

add_action('admin_menu', 'my_rest_api_plugin_test_page');

function my_rest_api_plugin_test_callback()
{
    ?>
    <div class="wrap">
        <h1>REST API TEST</h1>

        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row"><label for="new_rest_id">ID del post</label></th>
                    <td><input name="new_id" type="text" id="new_rest_id" value="" class="regular-text"></td>
                </tr>
                <tr>
                    <th scope="row"><label for="new_rest_title">Nuevo título</label></th>
                    <td><input name="title" type="text" id="new_rest_title" value="" class="regular-text"></td>
                </tr>
            </tbody>
        </table>
        <p class="submit">
            <input type="button" name="submit" id="go_rest_api" class="button button-primary" value="Guardar">
        </p>
        <p>
            <code id="rest_result">
            </code>
        </p>

    </div>

    <?php
}